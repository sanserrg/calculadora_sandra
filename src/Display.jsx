import React from 'react';
import { Alert } from 'reactstrap';

const Display = (props) => {

    let operad = {
        backgroundColor: '#d4edda',
        height: '15px',
        width: '225px',
        textAlign: 'left',
        fontSize: 'small',
        position: 'absolute',
        bottom: '0',
        left: '0'
    }

    let estilo = {
        width: '225px',
        height: '50px',
    }
    return (
        <Alert style={estilo} display={props.display}>{props.display}
            <div style={operad}>{props.esquina}</div>
        </Alert>
    )
}

export default Display;