import { React, useState } from 'react';
import "./App.css"
import { Container, Row, Col } from 'reactstrap';
import Boton from './Boton';
import Display from './Display';

export default () => {

  const [valor, setValor] = useState('')
  const [operador, setOperador] = useState('')
  const [display, setDisplay] = useState('')
  const [esquina, setEsquina] = useState('')
  const [fin, setFin] = useState(false)

  let nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
  let operadores = ['+', '-', '/', '*']

  let estilo = {
    width: '225px'
  }


  const pulsa = (x) => {
    if (nums.includes(x)) { // Si pulsa un número
      setDisplay(display + x)
      setEsquina(esquina + x)
      setFin(false)
    } else if (operadores.includes(x)) { // Si pulsa un operador
      setOperador(operador + x)
      setEsquina(esquina + x)
      setValor(display)
      setDisplay('')
      setFin(false)
    } else if (x === '=') { // Si pulsa para saber el total
      if (operador === '+') {
        setDisplay(valor*1 + display*1)
        setEsquina(esquina + x + (valor*1 + display*1))
        setFin(true)
      } else if (operador === '-') {
        setDisplay(valor*1 - display*1)
        setEsquina(esquina + x + (valor*1 - display*1))
        setFin(true)
      } else if (operador === '*') {
        setDisplay(valor*1 * display*1)
        setEsquina(esquina + x + (valor*1 * display*1))
        setFin(true)
      } else if (operador === '/') {
        setDisplay(valor*1 / display*1)
        setEsquina(esquina + x + (valor*1 / display*1))
        setFin(true)
      }
    } else if (x === 'C') { // Limpia pantalla
      setValor('')
      setOperador('')
      setDisplay('')
      setEsquina('')
      setFin(false)
    }
  }


  return (
    <div className="App">
      <Container style={estilo}>
        <Row><Col><h2>CALCULADORA</h2></Col></Row>
        <Row>
          <Col xs={12}><Display display={display} esquina={esquina}></Display></Col>
        </Row>
        <Row>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='1'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='2'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='3'></Boton></Col>
          <Col xs={3}><Boton tipo='op' fin={fin} display={display} pulsa={pulsa} valor='*'></Boton></Col>
        </Row>
        <Row>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='4'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='5'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='6'></Boton></Col>
          <Col xs={3}><Boton tipo='op' fin={fin} display={display} pulsa={pulsa} valor='/'></Boton></Col>
        </Row>
        <Row>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='7'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='8'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='9'></Boton></Col>
          <Col xs={3}><Boton tipo='op' fin={fin} display={display} pulsa={pulsa} valor='-'></Boton></Col>
        </Row>
        <Row>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='0'></Boton></Col>
          <Col xs={3}><Boton fin={fin} display={display} pulsa={pulsa} valor='C'></Boton></Col>
          <Col xs={3}><Boton tipo='op' fin={fin} display={display} pulsa={pulsa} valor='='></Boton></Col>
          <Col xs={3}><Boton tipo='op' fin={fin} display={display} pulsa={pulsa} valor='+'></Boton></Col>
        </Row>
      </Container>
    </div>
  )
}
