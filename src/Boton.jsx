import React from 'react';
import { Button } from 'reactstrap';

const Boton = (props) => {
    let estilo = {
        width: '56px',
        height: '56px'
    }
    return (
        <Button disabled = {(props.display === '' && props.tipo === 'op') || (props.fin && props.tipo === 'op')} style={estilo} onClick={() => props.pulsa(props.valor)}>{props.valor}</Button>
    )
}

export default Boton;